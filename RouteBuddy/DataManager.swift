//
//  DataManager.swift
//  RouteBuddy
//
//  Created by Christian Pomales on 7/15/16.
//  Copyright © 2016 Christian Pomales. All rights reserved.
//

import MapKit
import UIKit

class DataManager {
	var annotations = [CustomMKAnnotation]()
	static let sharedInstance = DataManager()
	
	// saves our annotations
	func saveData() {
		let saveData = NSKeyedArchiver.archivedDataWithRootObject(annotations)
		let defaults = NSUserDefaults.standardUserDefaults()
		defaults.setObject(saveData, forKey: "MapAnnotations")
		defaults.synchronize()
	}
}
