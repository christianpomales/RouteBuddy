//
//  CustomMKAnnotation.swift
//  RouteBuddy
//
//  Created by Christian Pomales on 7/4/16.
//  Copyright © 2016 Christian Pomales. All rights reserved.
//

import AddressBook
import Foundation
import MapKit

// allows us to set a custom uuid for each annotation
class CustomMKAnnotation: NSObject, NSCoding, MKAnnotation {
	var identifier: NSUUID
	var coordinate: CLLocationCoordinate2D
	var title: String?
	var subtitle: String?
	var routeNumer: Int
	
	init(identifier: NSUUID, coordinate: CLLocationCoordinate2D)
	{
		self.identifier = identifier
		self.coordinate = coordinate
		self.routeNumer = 0
		
		super.init()
	}
	
	required init(coder aDecoder: NSCoder) {
		identifier = aDecoder.decodeObjectForKey("identifier") as! NSUUID
		
		let coordinateLat = aDecoder.decodeObjectForKey("coordinateLatitude") as! CLLocationDegrees
		let coordinateLong = aDecoder.decodeObjectForKey("coordinateLongitude") as! CLLocationDegrees
		let decodedCoordinate = CLLocationCoordinate2D(latitude: coordinateLat, longitude: coordinateLong)
		coordinate = decodedCoordinate
		
		title = aDecoder.decodeObjectForKey("title") as? String
		subtitle = aDecoder.decodeObjectForKey("subtitle") as? String
		routeNumer = aDecoder.decodeObjectForKey("routeNumer") as! Int
	}
	
	func encodeWithCoder(aCoder: NSCoder) {
		aCoder.encodeObject(identifier, forKey: "identifier")
		aCoder.encodeObject(coordinate.latitude, forKey: "coordinateLatitude")
		aCoder.encodeObject(coordinate.longitude, forKey: "coordinateLongitude")
		aCoder.encodeObject(title, forKey: "title")
		aCoder.encodeObject(subtitle, forKey: "subtitle")
		aCoder.encodeObject(routeNumer, forKey: "routeNumer")
	}
	
}
