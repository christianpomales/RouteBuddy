//
//  DetailTableViewController.swift
//  RouteBuddy
//
//  Created by Christian Pomales on 7/11/16.
//  Copyright © 2016 Christian Pomales. All rights reserved.
//

import MapKit
import UIKit

class DetailTableViewController: UITableViewController {
	var mapItems = [MKMapItem]()
	
	override func viewDidLoad() {
		super.viewDidLoad()
		tableView.dataSource = self
		tableView.delegate = self
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	
	override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		let numberOfRows = mapItems.count
		print("\(numberOfRows)")
		return numberOfRows
	}
	
	override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCellWithIdentifier("DetailCell")!
		if let selectedItemName = mapItems[indexPath.row].name {
			cell.textLabel?.text = " \(indexPath.row + 1). \t \(selectedItemName)"
		}
		return cell
	}
	
	// takes the user to apple maps
	override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
		let destination = mapItems[indexPath.row]
		let launchOptions = [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving]
		destination.openInMapsWithLaunchOptions(launchOptions)
	}
	
	// returns our array of map items
	func initializeMapItems(mapItems: [MKMapItem]) {
		self.mapItems = mapItems
	}
}
