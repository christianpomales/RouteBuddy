//
//  MapHelper.swift
//  RouteBuddy
//
//  Created by Christian Pomales on 7/7/16.
//  Copyright © 2016 Christian Pomales. All rights reserved.
//

import MapKit

var drawCounter = 0
var itemsInLocationArray: Int?
var routingComplete = false

func calculateDirections(constLocationArray: [MKMapItem], VarLocationArray: [MKMapItem], startingLocation: MKMapItem, routes: [(MKDirectionsResponse, MKRoute)], drawRoute: (route: MKRoute, destinationMapItem: MKMapItem, routingComplete: Bool) -> Void) {
	// make sure we're not going out of our array bounds
	guard let destination = VarLocationArray.first else { return }
	
	// setup our itemsInLocationArray var
	if itemsInLocationArray == 0 || itemsInLocationArray == nil {
		itemsInLocationArray = constLocationArray.count
	}
	
	// get our directions
	let directions = getDirections(startingLocation, destination: destination)
	
	// calculate directions
	directions.calculateDirectionsWithCompletionHandler { (response, error) in
		guard let directionsResponse = response else { return }
		guard let routeResponse = directionsResponse.routes.first else { return }
		
		var routesCalculated = routes
		routesCalculated.append((directionsResponse, routeResponse))
		
		// if we have completed the entire VarLocationArray
		if routesCalculated.count == constLocationArray.count {
			// get the fastest route
			let fastestRoute = routesCalculated.sort({ $0.1.expectedTravelTime < $1.1.expectedTravelTime })[0]
			
			// ----------------debug----------------
			drawCounter += 1
			print("drawRoute called \(drawCounter) times")
			// -------------------------------------
			
			// check to see if we've finished
			if itemsInLocationArray == drawCounter {
				routingComplete = true
			}
			
			// draw our route
			drawRoute(route: fastestRoute.1, destinationMapItem: fastestRoute.0.destination, routingComplete: routingComplete)
			
			// remove our destination from the array of MKMapItems
			var newLocationArray = constLocationArray
			for (index, item) in newLocationArray.enumerate() {
				let locationArrayLat = item.placemark.coordinate.latitude
				let locationArrayLong = item.placemark.coordinate.longitude
				let fastestRouteLat = fastestRoute.0.destination.placemark.coordinate.latitude
				let fastestRouteLong = fastestRoute.0.destination.placemark.coordinate.longitude
				if locationArrayLat == fastestRouteLat && locationArrayLong == fastestRouteLong {
					newLocationArray.removeAtIndex(index)
				}
			}
			
			// set our new starting location
			let newStartingLocation = fastestRoute.0.destination
			
			// run again with the updated constLocationArray of MKMapItems
			calculateDirections(newLocationArray, VarLocationArray: newLocationArray, startingLocation: newStartingLocation, routes: [(MKDirectionsResponse, MKRoute)](), drawRoute: drawRoute)
		} else {
			// if we have not completed the VarLocationArray
			var newLocationArray = VarLocationArray
			newLocationArray.removeFirst()
			
			// run again with the same constLocationArray of MKMapItems
			calculateDirections(constLocationArray, VarLocationArray: newLocationArray, startingLocation: startingLocation, routes: routesCalculated, drawRoute: drawRoute)
		}
	}
}

// get directions between two points
func getDirections(startingLocation: MKMapItem, destination: MKMapItem) -> MKDirections {
	let directionsRequest = MKDirectionsRequest()
	directionsRequest.source = startingLocation
	directionsRequest.destination = destination
	directionsRequest.requestsAlternateRoutes = false
	directionsRequest.transportType = .Automobile
	
	let directions = MKDirections(request: directionsRequest)
	
	return directions
}