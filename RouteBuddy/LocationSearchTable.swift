//
//  LocationSearchTable.swift
//  RouteBuddy
//
//  Created by Christian Pomales on 7/2/16.
//  Copyright © 2016 Christian Pomales. All rights reserved.
//

import MapKit
import UIKit

// table for our search results
class LocationSearchTable: UITableViewController {
	// variables
	var matchingItems: [MKMapItem] = []
	var mapView: MKMapView? = nil
	var handleMapSearchDelegate: HandleMapSearch? = nil
	
	// parses the adress returned from the MapKit API into something more readable
	func parseAddress(selectedItem: MKPlacemark) -> String {
		// put a space between "1" and "Infinite Loop"
		let firstSpace = (selectedItem.subThoroughfare != nil && selectedItem.thoroughfare != nil) ? " " : ""
		// put a comma between street and city/state
		let comma = (selectedItem.subThoroughfare != nil || selectedItem.thoroughfare != nil) && (selectedItem.subAdministrativeArea != nil || selectedItem.administrativeArea != nil) ? ", " : ""
		// put a space between "Washington" and "DC"
		let secondSpace = (selectedItem.subAdministrativeArea != nil && selectedItem.administrativeArea != nil) ? " " : ""
		let addressLine = String(
			format: "%@%@%@%@%@%@%@",
			// street number
			selectedItem.subThoroughfare ?? "",
			firstSpace,
			// street name
			selectedItem.thoroughfare ?? "",
			comma,
			// city
			selectedItem.locality ?? "",
			secondSpace,
			// state
			selectedItem.administrativeArea ?? ""
		)
		return addressLine
	}
}

// displays
extension LocationSearchTable {
	// numberOfRowsInSection
	override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return matchingItems.count
	}
	
	// cellForRowAtIndexPath
	override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCellWithIdentifier("cell")!
		let selectedItem = matchingItems[indexPath.row].placemark
		cell.textLabel?.text = selectedItem.name
		cell.detailTextLabel?.text = parseAddress(selectedItem)
		return cell
	}
}

// handles updating out search results
extension LocationSearchTable: UISearchResultsUpdating {
	// update our search results as we type
	func updateSearchResultsForSearchController(searchController: UISearchController) {
		// optionally unwraps mapView and searchBarText
		guard let mapView = mapView,
			let searchBarText = searchController.searchBar.text else { return }
		
		// setup search requests
		let request = MKLocalSearchRequest()
		request.naturalLanguageQuery = searchBarText
		request.region = mapView.region
		
		// search, reloading as we type
		let search = MKLocalSearch(request: request)
		search.startWithCompletionHandler { response, _ in
			guard let response = response else { return }
			
			self.matchingItems = response.mapItems
			self.tableView.reloadData()
		}
		
	}
}

// handles displaying our pin from our selected search item
extension LocationSearchTable {
	override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
		let selectedItem = matchingItems[indexPath.row].placemark
		handleMapSearchDelegate?.dropPinZoomIn(selectedItem)
		dismissViewControllerAnimated(true, completion: nil)
	}
}