//
//  ViewController.swift
//  RouteBuddy
//
//  Created by Christian Pomales on 7/2/16.
//  Copyright © 2016 Christian Pomales. All rights reserved.
//

import MapKit
import UIKit

protocol HandleMapSearch {
	func dropPinZoomIn(placemark: MKPlacemark)
}

class ViewController: UIViewController {
	// variables
	let locationManager = CLLocationManager()
	var resultSearchController: UISearchController? = nil
	var mapDestinations = [MKMapItem]()
	var routeNumberCounter = 1
	
	// outlets
	@IBOutlet weak var mapView: MKMapView!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		// setup location manager
		locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
		locationManager.desiredAccuracy = kCLLocationAccuracyBest
		locationManager.requestLocation()
		
		// setup search results table
		let locationSearchTable = storyboard!.instantiateViewControllerWithIdentifier("LocationSearchTable") as! LocationSearchTable
		locationSearchTable.mapView = mapView
		locationSearchTable.handleMapSearchDelegate = self
		
		// setup result search controller
		resultSearchController = UISearchController(searchResultsController: locationSearchTable)
		resultSearchController?.searchResultsUpdater = locationSearchTable
		resultSearchController?.hidesNavigationBarDuringPresentation = false
		resultSearchController?.dimsBackgroundDuringPresentation = true
		
		// setup our search bar
		let searchBar = resultSearchController!.searchBar
		searchBar.sizeToFit()
		searchBar.placeholder = "Search for waypoints"
		navigationItem.titleView = resultSearchController?.searchBar
		definesPresentationContext = true
		
		// setup our toolbar
		let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .FlexibleSpace, target: nil, action: nil)
		let resetLocation = UIBarButtonItem(image: UIImage(named: "navigation"), style: .Plain, target: self, action: #selector(locationReset))
		let clearMap = UIBarButtonItem(barButtonSystemItem: .Trash, target: self, action: #selector(clearMapAlert))
		let calculateRoute = UIBarButtonItem(title: "Calculate", style: .Plain, target: self, action: #selector(displayRoute))
		
		toolbarItems = [resetLocation, flexibleSpace, calculateRoute, flexibleSpace, clearMap]
		navigationController?.setToolbarHidden(false, animated: false)
		
		// get our location
		locationManager.location
		
		// add our saved annotations
		let defaults = NSUserDefaults.standardUserDefaults()
		if let token = defaults.objectForKey("MapAnnotations") as? NSData {
			let annotations = NSKeyedUnarchiver.unarchiveObjectWithData(token) as! [CustomMKAnnotation]
			mapView.addAnnotations(annotations)
		}
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	
	override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
		if segue.identifier == "transitionToTableView" {
			let viewController = segue.destinationViewController as! DetailTableViewController
			viewController.initializeMapItems(mapDestinations)
		}
	}
	
	// get drections for the selected pin
	func getDirections() {
		if let selectedAnnotation = mapView.selectedAnnotations.first as? CustomMKAnnotation {
			for mapViewAnnotation in mapView.annotations {
				if let convertedMapViewAnnotation = mapViewAnnotation as? CustomMKAnnotation {
					if selectedAnnotation.identifier == convertedMapViewAnnotation.identifier {
						// create a name to be showin in apple maps
						let placemarkName = ["Name": selectedAnnotation.title ?? ""]
						// create a placemark to make into a MKMapItem
						let annotationToPlacemark = MKPlacemark(coordinate: selectedAnnotation.coordinate, addressDictionary: placemarkName)
						let mapItem = MKMapItem(placemark: annotationToPlacemark)
						let launchOptions = [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving]
						mapItem.openInMapsWithLaunchOptions(launchOptions)
						
					}
				}
			}
		}
	}
	
	// clears all pins from the view
	func clearMap() {
		mapView.removeAnnotations(mapView.annotations)
		mapView.removeOverlays(mapView.overlays)
		mapDestinations.removeAll()
		
		// save our data
		saveData()
	}
	
	// remove a single pin from the view
	func removePin() {
		if let selectedAnnotation = mapView.selectedAnnotations.first as? CustomMKAnnotation {
			for mapViewAnnotation in mapView.annotations {
				if let convertedMapViewAnnotation = mapViewAnnotation as? CustomMKAnnotation {
					if selectedAnnotation.identifier == convertedMapViewAnnotation.identifier {
						mapView.removeAnnotation(selectedAnnotation)
						
						// save our data
						saveData()
					}
				}
			}
		}
	}
	
	// presents an alert to clear the entire map
	func clearMapAlert() {
		// create alert controller
		let clearMapAlert = UIAlertController(title: "Clear all waypoints?", message: nil, preferredStyle: .Alert)
		// create ok button
		let okClearMapAlertButton = UIAlertAction(title: "OK", style: .Default) { (clearMapAlert: UIAlertAction!) in
			self.clearMap()
		}
		// create cancel button
		let cancelClearMapAlertButton = UIAlertAction(title: "Cancel", style: .Default, handler: nil)
		
		// add buttons
		clearMapAlert.addAction(okClearMapAlertButton)
		clearMapAlert.addAction(cancelClearMapAlertButton)
		
		// present alert
		presentViewController(clearMapAlert, animated: true, completion: nil)
	}
	
	// takes the user back to their location on the map
	func locationReset() {
		let lastLocation = locationManager.location
		
		// zoom into user's location
		if let location = lastLocation {
			let span = MKCoordinateSpanMake(0.05, 0.05)
			let region = MKCoordinateRegion(center: location.coordinate, span: span)
			mapView.setRegion(region, animated: true)
		}
	}
	
	// gets an optimal route to all our waypoints and then displays them
	func displayRoute() {
		// remove any overlays
		mapView.removeOverlays(mapView.overlays)
		mapDestinations.removeAll()
		
		// reset our routeCounter number
		routeNumberCounter = 1
		
		// clear our routingComplete flag
		routingComplete = false
		
		let annotations = mapView.annotations
		var mapItems = createMKMapItemArrayFromMKAnnotations(annotations)
		
		guard let userLocation = locationManager.location?.coordinate else { return }
		let userLocationPlacemark = MKPlacemark(coordinate: userLocation, addressDictionary: nil)
		let userLocationMapItem = MKMapItem(placemark: userLocationPlacemark)
		
		// remove the users location
		for (index, item) in mapItems.enumerate() {
			let itemLat = item.placemark.coordinate.latitude
			let itemLong = item.placemark.coordinate.longitude
			let userLat = userLocationMapItem.placemark.coordinate.latitude
			let userLong = userLocationMapItem.placemark.coordinate.longitude
			if itemLat == userLat && itemLong == userLong {
				mapItems.removeAtIndex(index)
			}
		}
		
		// display an Activity Indicator
		let progressHUD = ProgressHUD(text: "Calculating")
		self.view.addSubview(progressHUD)
		
		calculateDirections(mapItems, VarLocationArray: mapItems, startingLocation: userLocationMapItem, routes: [(MKDirectionsResponse, MKRoute)]()) {
			(route, destinationMapItem, routingComplete) in
			
			self.addRouteOverlay(route)
			self.mapDestinations.append(destinationMapItem)
			
			// add numbers to the pins
			for annotation in self.mapView.annotations {
				let destinationMapItemLat = destinationMapItem.placemark.coordinate.latitude
				let destinationMapItemLong = destinationMapItem.placemark.coordinate.longitude
				let annotationLat = annotation.coordinate.latitude
				let annotationLong = annotation.coordinate.longitude
				
				if destinationMapItemLat == annotationLat && destinationMapItemLong == annotationLong {
					if let customAnnotation = annotation as? CustomMKAnnotation {
						customAnnotation.routeNumer = self.routeNumberCounter
						self.routeNumberCounter += 1
						self.mapView.removeAnnotation(annotation)
						self.mapView.addAnnotation(customAnnotation)
					}
				}
			}
			
			// hide out activity indicator when finished
			if routingComplete {
				// hide our progressHud
				progressHUD.hide()
				// reset some mapHelper state
				drawCounter = 0
				itemsInLocationArray = 0
				
				// get rid out our progressHud view
				for view in self.view.subviews {
					if view is ProgressHUD {
						view.removeFromSuperview()
					}
				}
			}
		}
	}
	
	// add directions overlay to map
	func addRouteOverlay(route: MKRoute) {
		// mapView.removeOverlays(mapView.overlays)
		self.mapView.addOverlay(route.polyline)
		self.mapView.setVisibleMapRect(route.polyline.boundingMapRect, animated: true)
	}
	
	func createMKMapItemArrayFromMKAnnotations(annotations: [MKAnnotation]) -> [MKMapItem] {
		var mapItemArray = [MKMapItem]()
		
		for annotation in annotations {
			// create a name to be showin in apple maps
			let placemarkName = ["Name": (annotation.title ?? "") ?? ""]
			// create a placemark to make into a MKMapItem
			let annotationToPlacemark = MKPlacemark(coordinate: annotation.coordinate, addressDictionary: placemarkName)
			let mapItem = MKMapItem(placemark: annotationToPlacemark)
			mapItemArray.append(mapItem)
		}
		
		return mapItemArray
	}
	
	// save any annotation data
	func saveData() {
		// data manager
		let dataManager = DataManager.sharedInstance
		
		var annotations = mapView.annotations
		
		if let userLocation = locationManager.location?.coordinate {
			let userLocationLat = userLocation.latitude
			let userLocationLong = userLocation.longitude
			
			for (index, annotation) in annotations.enumerate() {
				let annotationLat = annotation.coordinate.latitude
				let annotationLong = annotation.coordinate.longitude
				
				if userLocationLat == annotationLat && userLocationLong == annotationLong {
					annotations.removeAtIndex(index)
				}
			}
			
			dataManager.annotations = annotations as! [CustomMKAnnotation]
		}
		
		dataManager.saveData()
	}
}

// implement the CLLocationManagerDelegate protocol for our ViewController
extension ViewController: CLLocationManagerDelegate {
	func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
		// debug
		if status == .AuthorizedWhenInUse {
			locationManager.requestLocation()
		}
	}
	
	func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
		// zoom into user's location
		if let location = locations.first {
			let span = MKCoordinateSpanMake(0.05, 0.05)
			let region = MKCoordinateRegion(center: location.coordinate, span: span)
			mapView.setRegion(region, animated: true)
		}
		
	}
	
	func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
		// debug
		print("Error: \(error)")
	}
	
}

// implement the HandleMapSearch protocol for our ViewController
extension ViewController: HandleMapSearch {
	func dropPinZoomIn(placemark: MKPlacemark) {
		
		// prevent duplicate annotations from being created
		for annotation in mapView.annotations {
			let placemarkLat = placemark.coordinate.latitude
			let placemarkLong = placemark.coordinate.longitude
			let annotationLat = annotation.coordinate.latitude
			let annotationLong = annotation.coordinate.longitude
			
			if placemarkLat == annotationLat && placemarkLong == annotationLong {
				return
			}
		}
		
		let uuid = NSUUID()
		let annotation = CustomMKAnnotation(identifier: uuid, coordinate: placemark.coordinate)
		annotation.title = placemark.name
		
		if let city = placemark.locality,
			let state = placemark.administrativeArea {
				annotation.subtitle = "\(city) \(state)"
		}
		
		mapView.addAnnotation(annotation)
		
		// save our data
		saveData()
		
		let span = MKCoordinateSpanMake(0.05, 0.05)
		let region = MKCoordinateRegionMake(placemark.coordinate, span)
		mapView.setRegion(region, animated: true)
	}
	
}

extension ViewController: MKMapViewDelegate {
	// wire buttons to the pin's callout
	func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView? {
		if annotation is MKUserLocation {
			return nil
		}
		
		let reuseID = "pin"
		var pinView = mapView.dequeueReusableAnnotationViewWithIdentifier(reuseID) as? MKPinAnnotationView
		pinView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: reuseID)
		pinView?.pinTintColor = UIColor.redColor()
		pinView?.canShowCallout = true
		
		// label for pin
		let label = UILabel(frame: CGRect(x: -5, y: -10, width: 25, height: 25))
		label.layer.masksToBounds = true
		label.layer.cornerRadius = label.frame.width / 2
		label.textAlignment = .Center
		label.backgroundColor = UIColor.redColor()
		label.textColor = UIColor.whiteColor()
		
		if let customAnnotation = annotation as? CustomMKAnnotation {
			let routeNumber = customAnnotation.routeNumer
			if routeNumber != 0 {
				label.text = "\(routeNumber)"
			}
		}
		
		pinView?.addSubview(label)
		// -------------
		
		let smallSquare = CGSize(width: 30, height: 30)
		
		// route button
		let routeButton = UIButton(frame: CGRect(origin: CGPointZero, size: smallSquare))
		routeButton.setBackgroundImage(UIImage(named: "car"), forState: .Normal)
		routeButton.addTarget(self, action: #selector(getDirections), forControlEvents: .TouchUpInside)
		
		// remove button
		let removeButton = UIButton(frame: CGRect(origin: CGPointZero, size: smallSquare))
		removeButton.setBackgroundImage(UIImage(named: "remove"), forState: .Normal)
		removeButton.addTarget(self, action: #selector(removePin), forControlEvents: .TouchUpInside)
		
		pinView?.leftCalloutAccessoryView = routeButton
		pinView?.rightCalloutAccessoryView = removeButton
		return pinView
	}
	
	// draw the overlay for the route
	func mapView(mapView: MKMapView, rendererForOverlay overlay: MKOverlay) -> MKOverlayRenderer {
		let renderer = MKPolylineRenderer(polyline: overlay as! MKPolyline)
		renderer.strokeColor = UIColor(red: 0.00, green: 0.48, blue: 1.00, alpha: 0.75)
		return renderer
	}
}